# cURL

Curl is a command line tool for transferring data specified with URL syntax.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## CURL.LSM

<table>
<tr><td>title</td><td>cURL</td></tr>
<tr><td>version</td><td>8.5.0</td></tr>
<tr><td>entered&nbsp;date</td><td>2024-11-04</td></tr>
<tr><td>description</td><td>Command line tool for transferring data</td></tr>
<tr><td>summary</td><td>Curl is a command line tool for transferring data specified with URL syntax.</td></tr>
<tr><td>keywords</td><td>freedos, curl</td></tr>
<tr><td>author</td><td>Curl team</td></tr>
<tr><td>maintained&nbsp;by</td><td>Michael Kostylev</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/curl/</td></tr>
<tr><td>original&nbsp;site</td><td>http://curl.haxx.se http://curl.se</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://mik.dyndns.pro/dos-stuff/</td></tr>
<tr><td>platforms</td><td>FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[Open source](LICENSE)</td></tr>
<tr><td>packager</td><td>Eric Auer</td></tr>
</table>
